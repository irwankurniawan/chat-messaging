package com.example.chatmessaging;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    Context ctx;
    ArrayList<Chat> listChats;

    public Adapter(Context ctx, ArrayList<Chat> listChats) {
        this.ctx = ctx;
        this.listChats = listChats;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(ctx).inflate(R.layout.chat_list, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Chat chat = listChats.get(position);

        holder.ivThumbnail.setImageResource(chat.thumbnail);
        holder.tvName.setText(chat.name);
        holder.tvChat.setText(chat.message);
    }

    @Override
    public int getItemCount() {
        return listChats.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView ivThumbnail;
        TextView tvName;
        TextView tvChat;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivThumbnail = itemView.findViewById(R.id.ivProfilePicture);
            tvName = itemView.findViewById(R.id.tvName);
            tvChat = itemView.findViewById(R.id.tvChat);
        }

    }
}
